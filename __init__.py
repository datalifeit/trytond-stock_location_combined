# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import location


def register():
    Pool.register(
        location.Combined,
        location.Location,
        location.LocationCombined,
        module='stock_location_combined', type_='model')
