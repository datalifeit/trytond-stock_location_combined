# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
from trytond.exceptions import UserError
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool


class LocationCombinedTestCase(ModuleTestCase):
    """Test combined location module"""
    module = 'stock_location_combined'

    @with_transaction()
    def test_combined(self):
        """Create combined location"""
        pool = Pool()
        Location = pool.get('stock.location')
        Combined = pool.get('stock.location.combined')

        location1, = Location.create([
            {'name': 'Production 1',
             'type': 'production',
             'active': True}
        ])
        combined1, = Combined.create([
            {'name': 'Combined 1',
             'active': True,
             'locations': [('add', [location1.id])]}
        ])
        self.assert_(combined1.id)
        self.assertEqual(combined1.type, location1.type)

    @with_transaction()
    def test_combined_domain(self):
        """Add wrong location type to combined location"""
        pool = Pool()
        Location = pool.get('stock.location')
        Combined = pool.get('stock.location.combined')

        location1, = Location.create([
            {'name': 'Production 1',
             'type': 'production',
             'active': True}
        ])
        combined1, = Combined.create([
            {'name': 'Combined 1',
             'active': True,
             'locations': [('add', [location1.id])]}
        ])
        location2, = Location.create([
            {'name': 'Storage 1',
             'type': 'storage',
             'active': True}
        ])
        self.assertRaises(UserError, Combined.write,
                          [combined1],
                          {'locations': [('add', [location2.id])]})

    @with_transaction()
    def test_location_type(self):
        pool = Pool()
        Location = pool.get('stock.location')
        Combined = pool.get('stock.location.combined')

        location1, = Location.create([
            {'name': 'Production 1',
             'type': 'production',
             'active': True}
        ])
        combined1, = Combined.create([
            {'name': 'Combined 1',
             'active': True,
             'locations': [('add', [location1.id])]}
        ])
        location3, = Location.create([
            {'name': 'Production 2',
             'type': 'production',
             'active': True}
        ])
        Combined.write([combined1], {
            'locations': [('add', [location3.id])]
        })
        self.assertRaises(UserError, Location.write,
                          [location1],
                          {'type': 'storage'})


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(LocationCombinedTestCase))
    return suite
