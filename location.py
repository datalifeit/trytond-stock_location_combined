# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Combined(DeactivableMixin, ModelView, ModelSQL):
    """Combined location"""
    __name__ = 'stock.location.combined'

    name = fields.Char('Name', required=True, translate=True,
                       states={'readonly': ~Eval('active')},
                       depends=['active'])
    code = fields.Char('Code')
    type = fields.Function(
        fields.Selection([], 'Locations type'),
        'on_change_with_type')
    locations = fields.Many2Many('stock.location-stock.location.combined',
                                 'combined', 'location', 'Locations',
                                 states={'readonly': ~Eval('active')},
                                 depends=['active'])

    @classmethod
    def __setup__(cls):
        pool = Pool()
        Location = pool.get('stock.location')

        super(Combined, cls).__setup__()
        if not cls.type._field.selection:
            cls.type._field.selection.extend(Location.type.selection)
        cls.locations.domain.append(
            ('type', 'in', cls._locations_domain()))

    @classmethod
    def _locations_domain(cls):
        return ['production']

    @fields.depends('locations')
    def on_change_with_type(self, name=None):
        if self.locations:
            return self.locations[0].type
        return 'production'

    def _get_locations(self):
        return self.locations

    @classmethod
    def validate(cls, records):
        super(Combined, cls).validate(records)
        _types = {}
        for record in records:
            values = [l.type for l in record.locations]
            _types.setdefault(record.id, set(values))
            if len(_types[record.id]) > 1:
                raise UserError(gettext(
                    'stock_location_combined.msg_location_combined_type',
                    location=record.rec_name))


class LocationCombined(ModelView, ModelSQL):
    """Location - Combined location"""
    __name__ = 'stock.location-stock.location.combined'
    _table = 'stock_location_combined_rel'

    combined = fields.Many2One('stock.location.combined', 'Combined',
                               ondelete='CASCADE')
    location = fields.Many2One('stock.location', 'Location',
                               ondelete='RESTRICT')


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    combined = fields.Many2Many('stock.location-stock.location.combined',
                                'location', 'combined', 'Combined locations')

    @classmethod
    def validate(cls, records):
        super(Location, cls).validate(records)
        for record in records:
            if not record.combined:
                continue
            for _combined in record.combined:
                if any(l.type != record.type for l in _combined.locations):
                    raise UserError(gettext(
                        'stock_location_combined.msg_location_combined_type',
                        location=_combined.rec_name))
